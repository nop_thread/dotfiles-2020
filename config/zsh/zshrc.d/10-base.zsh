# Set `$LANG`. {{{1
() {
	# Force `LANG=C` for root.
	if (( $EUID == 0 )) ; then
		LANG=C
		return
	fi

	# Use default on terminal multiplexer.
	# This is because the multiplexer will be attached from various terminal emulators.
	local multiplexer
	() {
		if [[ -n $TMUX ]] ; then
			multiplexer=tmux
			return
		fi
		case $TERM in
			tmux|tmux-*)
				multiplexer=tmux
				return
				;;
			screen|screen-*)
				multiplexer=screen
				return
				;;
		esac
	}
	if [[ -n $multiplexer ]] ; then
		# Running on terminal multiplexer.
		return
	fi

	# Use `LANG=C` for agetty.
	if [[ $TERM == linux ]] ; then
		LANG=C
		return
	fi

	# If no special rules are applicable, use default.
	return
}
export LANG

# Prepare setting basic environment variables. {{{1
#
# NOTE:
# Man page tell us to use `typeset -U PATH path`, but this does
# not work in `for` loop.
# It is necessary to explicitly make them global, by doing
# `typeset -gU PATH path`, `typeset -xU PATH path`, or something
# like that.
# See <https://mastodon.cardina1.red/@lo48576/99816229749927704>
# for detail.
() {
	setopt localoptions glob_assign
	typeset -gU PATH path

	typeset -xT LD_LIBRARY_PATH ld_library_path
	typeset -U LD_LIBRARY_PATH ld_library_path

	typeset -xT LD_PRELOAD ld_preload
	typeset -U LD_PRELOAD ld_preload

	typeset -gxU INCLUDE include
	export INCLUDE
}

# Set `$SUDO_PATH`.
if (( $EUID == 0 )) ; then
	typeset -xT SUDO_PATH sudo_path
	typeset -U SUDO_PATH sudo_path
	sudo_path=(
		/usr/local/sbin(N-/)
		/usr/sbin(N-/)
		/sbin(N-/)
		$sudo_path
		)
	sudo_path=( ${^sudo_path}(N-/^W) )
	path+=( $sudo_path )
fi

# Set `$MANPATH`.
typeset -gU MANPATH manpath

# }}}1

# vim: set foldmethod=marker :
