# youtube-dl

if (( $+commands[youtube-dl] )) ; then
	# `youtube-dl`.
	alias youtube-dl="noglob youtube-dl --xattrs --audio-quality=0"
	alias twitter-dl="noglob youtube-dl --xattrs --audio-quality=0 --config-location ${HOME}/.config/youtube-dl/config-twitter"
fi
