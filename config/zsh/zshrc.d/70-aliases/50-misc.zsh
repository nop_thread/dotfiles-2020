# misc, basic wellknown or popular commands

# Commands which should be run with `LANG=C` {{{1

# Always show man page in English.
alias cman="LANG='C' man"

# mv, cp, rm {{{1

# Prevent overwriting files by accident.
alias mv="LANG=C mv -i"
alias cp="LANG=C cp -i"
# See <https://wiki.archlinux.org/index.php/Core_utilities#rm>.
alias rm="LANG=C rm -I --one-file-system"

# du, df {{{1

# -h (--human-readable): print sizes in human readable format
alias du="du -h"
alias df="df -h"

# ps {{{1

# a: Lift the BSD-style "only yourself" restriction
# u: Display user-oriented format
# x: Lift the BSD-style "must have a tty" restriction
#
# NOTE: Do not use `pax`: pax is POSIX command!
# See <https://pubs.opengroup.org/onlinepubs/9699919799/utilities/pax.html>.
alias pa="ps aux"

# Text searcher {{{1

# grep
alias grep="grep --color=auto"

# sudo, su {{{1

# Enable alias expansion of commands after sudo command with a space after "sudo".
alias sudo="sudo "

# -l: use a login shell
alias su="su -l"

# git {{{1

alias g="git"

# }}}1

# vim: foldmethod=marker :
