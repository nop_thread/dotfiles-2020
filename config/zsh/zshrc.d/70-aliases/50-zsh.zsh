# Zsh, and built-in commands.

# zmv {{{1

autoload -Uz zmv
alias zmv="noglob zmv"
alias zmvw="noglob zmv -W"
alias zcp="noglob zmv -C"
alias zcpw="noglob zmv -C -W"
alias zln="noglob zmv -L"
alias zlnw="noglob zmv -L -W"

# history {{{1

alias hist-all="history -E 1"
alias histgrep="history -E 1 | grep"

# misc {{{1

alias where="command -v"
alias j="jobs -l"

# restart zsh
alias zz="exec zsh"

# Quit zsh like vim.
alias :q="exit"
# I sometimes press 'q' with Shift because ':' is Shift-; ...
alias :Q="exit"

# }}}1

# vim: set foldmethod=marker :
