# My commands

# autmux {{{1
if (( $+commands[autmux] )) ; then
	alias ::='autmux '
fi

# sync {{{1
# Don't sync, feel.
alias feel="sync ; sync ; sync"

# }}}1

# vim: set foldmethod=marker :
