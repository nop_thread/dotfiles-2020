# rlwrap

if (( $+commands[rlwrap] )) ; then
	if (( $+commands[ocaml] )) ; then
		alias ocaml="rlwrap ocaml"
	fi
	if (( $+commands[gosh] )) ; then
		alias gosh="rlwrap gosh"
	fi
fi
