# silicon
if (( $+commands[silicon] )) ; then
	# `silicon`.
	# Without explicit font argument, Japanese characters are not correctly rendered.
	alias silicon="silicon -f 'monospace'"
fi
