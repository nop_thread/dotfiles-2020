# ls.

# Basic aliases {{{1
() {
	# --time-style=long-iso : "+%Y-%m-%d %H:%M" format
	typeset -a common_opts
	common_opts=(
		--time-style=long-iso
		)
	case "${OSTYPE:-}" in
	linux*)
		alias ls="ls --color=auto ${(q)common_opts}"
		;;
	freebsd*|darwin*)
		alias ls="ls -G -w ${(q)common_opts}"
		;;
	esac

	# In case of typo.
	alias l="ls"
	alias s="ls"
}

# Alias with options {{{1

# -a (--all): list entries starting with .
alias la="ls -a"
# -F (--classify): append file type indicators
alias lf="ls -F"
# -l: long listing
alias ll="ls -l"
# -h (--human-readable): print sizes in human readable form
alias llh="ls -lh"
alias lhl="ls -lh"

alias lla="ls -la"
alias lal="ls -la"
alias llah="ls -lha"
alias llha="ls -lha"

# }}}1

# vim: set foldmethod=marker :
