" Utility functions for my nvim rc.

" Key mapping with variable {{{1

function! s:MapKeyCommandline(mode, lhs, rhs, noremap)
	if a:noremap
		let mapcmd = 'noremap'
	else
		let mapcmd = 'map'
	endif
	return a:mode .. l:mapcmd .. ' ' .. a:lhs .. ' ' .. a:rhs
endfunction

"function! BulkMapKey(modes, lhs, rhs, noremap)
"    for l:mode in split(a:modes, '\zs')
"        execute MapKeyCommandline(l:mode, a:lhs, a:rhs, a:noremap)
"    endfor
"endfunction

function! MapKey(modes, lhs, rhs)
	for l:mode in split(a:modes, '\zs')
		execute s:MapKeyCommandline(l:mode, a:lhs, a:rhs, v:false)
	endfor
endfunction

"function! NoremapKey(modes, lhs, rhs)
"    for l:mode in split(a:modes, '\zs')
"        execute MapKeyCommandline(l:mode, a:lhs, a:rhs, v:true)
"    endfor
"endfunction

" }}}1

" vim: set foldmethod=marker :
