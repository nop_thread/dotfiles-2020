" Keys.

" Note that leader keys should be set earlier than plugins setup.

" Disable defaults {{{1

" Disable `<F1>` (built-in) help.
nnoremap <F1> <nop>
inoremap <F1> <nop>

" Disable `q:` history.
nnoremap q: <nop>
" Disable `Q` visual mode.
nnoremap Q <nop>

" Characters {{{1

inoremap <C-d> <Delete>

" Move {{{1

" Use F3 instead of C-j because input method with SKK captures C-j to switch
" input mode.
imap <F3> <C-j>
nmap <F3> <C-j>

inoremap <A-h> <Left>
inoremap <A-j> <Down>
inoremap <A-k> <Up>
inoremap <A-l> <Right>

" Use incremental search by C-n / C-p in command mode.
cnoremap <C-n> <Down>
cnoremap <C-p> <Up>

" Actions {{{1

" Buffer {{{2

nnoremap <silent> g<C-p> :bp<CR>
nnoremap <silent> g<C-n> :bn<CR>

" Close quickfix buffer by 'q' on the buffer.
autocmd myvimrc FileType qf nnoremap <silent><buffer><nowait> q :quit<CR>

" Mode {{{2

" Toggle paste mode.
set pastetoggle=<leader>p

" }}}1

" vim: set foldmethod=marker :
