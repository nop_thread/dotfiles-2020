" Filetype-specific syntax settings.

" Disable syntax for plain text. {{{1

" Long long line can make vim quite heavy.
" Disable syntax highlighting on plain text.
function! s:DisableSyntax()
	if &ft =~ 'help\|cmake\|asciidoc\|markdown'
		return
	endif
	set syntax=off
endfunction

augroup PlainTextSyntax
	autocmd!
	autocmd BufEnter,BufNewFile,BufRead *.txt call s:DisableSyntax()
augroup END

" Preference. {{{1

" Documents. {{{2

autocmd myvimrc FileType asciidoc setlocal expandtab tabstop=2
autocmd myvimrc Filetype markdown setlocal expandtab tabstop=2
autocmd myvimrc FileType tex setlocal expandtab tabstop=4
" Disable automatic break for long lines, because I frequently use Japanese.
autocmd myvimrc Filetype text setlocal textwidth=0 noexpandtab tabstop=4
autocmd myvimrc FileType xml setlocal tabstop=2

" Source codes. {{{2

autocmd myvimrc FileType sh setlocal noexpandtab tabstop=4
autocmd myvimrc FileType zsh setlocal noexpandtab tabstop=4

" Config formats. {{{2

autocmd myvimrc FileType yaml setlocal expandtab tabstop=2
autocmd myvimrc FileType toml setlocal expandtab tabstop=4

" }}}1

" vim: set foldmethod=marker :
