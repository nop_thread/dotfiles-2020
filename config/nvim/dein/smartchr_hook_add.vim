function! s:Comma1()
	inoremap <expr> , smartchr#one_of(', ', ',')
endfunction

function! s:Equal2()
	inoremap <expr> = smartchr#one_of(' = ', ' == ', '=')
endfunction

function! s:Equal3()
	inoremap <expr> = smartchr#one_of(' = ', ' == ', ' === ', '=')
endfunction

augroup SmartChr
	autocmd!
	"autocmd FileType c,cpp,rust call s:Comma1()
	"autocmd FileType c,cpp,rust,toml call s:Equal2()
	"autocmd FileType javascript call s:Equal3()
augroup END
