# Prepend user bin directory (`$XDG_BIN_HOME`, default is usually `~/.local/bin`) to `$PATH`.
PATH="${XDG_BIN_HOME:-"$(readlink -f "${XDG_DATA_HOME:-"${HOME}/.local/share"}/../bin")"}:${PATH}"
