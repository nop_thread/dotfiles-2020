# Shadow
shadow = false;

# Fading
fading = false;

# Other
#backend = "glx";
# Old xrender backend will (almost) fix the tearing issue on nvidia card,
# but the new xrender backend doesn't support vsync on nvidia card.
# See <https://github.com/yshui/picom/wiki/Vsync-Situation> for detail.
#backend = "xrender";

backend = "xr_glx_hybrid";
vsync-use-glfinish = true;

use-ewmh-active-win = true;
# See <https://github.com/chjj/compton/wiki/vsync-guide> and
# <https://wiki.archlinux.org/index.php/Picom#Screentearing_with_NVIDIA's_proprietary_drivers>.
vsync = true;
# `sw-opti` should be disabled for `vsync = "opengl"`.
sw-opti = false;

# GLX backend
glx-no-stencil = true;
# See <https://github.com/chjj/compton/wiki/perf-guide>.
glx-no-rebind-pixmap = true;
# This eases tearing issue.
unredir-if-possible = false;
# This may ease flickering and tearing.
# See <https://github.com/chjj/compton/issues/402>.
#use-damage = false;

# > On GLX backend, bad synchronization between X Render and GLX may lead to
# > some sort of lagging effect (especially with `nvidia-drivers`).
# > Use `--xrender-sync-fence` to deal with it.
#
# > Needed on nvidia-drivers with GLX backend for some users.
xrender-sync-fence = true;

wintypes: {
	dock = {
		fade = false;
		shadow = false;
	};
	dnd = {
		fade = false;
		shadow = false;
	};
	tooltip = {
		fade = false;
		shadow = false;
		opacity = 0.85;
		focus = true;
	};
};

# References:
#
# * <https://github.com/yshui/picom/wiki/Vsync-Situation>
# * <https://github.com/yshui/picom/wiki/NVIDIA-quirks>
