# This file would be loaded not by zsh, but by display managers (such as LXDM).

for file in "${XDG_CONFIG_HOME:-${HOME}/.config}"/profile.d/*.sh ; do
	source "$file"
done

export XSESSION=xsession

export PATH="${HOME}/.cargo/bin:${PATH}"

# vim: set ft=sh :
