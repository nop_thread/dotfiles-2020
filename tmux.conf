# Status line. {{{1
# For >tmux-3.1.

# Update every 4 sec.
set -qg status-interval 4

# Status line global style setting. {{{2
set -qg status-style "bg=white"
set -qga status-style "fg=blue"

# Left-side component of status line. {{{2

# Username.
set -qg @user-name "#(whoami)"
set -qgF status-left "#{@user-name}"

# Hostname.
set -qga status-left "@#H"

# Session name and window number.
set -qga status-left " [#S:#I]"

# Trim the left-side component when too long.
set -qg status-left-length 160

# Right-side component of status line. {{{2

# Pane title (truncate if over 48 chars).
set -qg status-right "#{=48:pane_title}"
# Load average (1min).
set -qga status-right " #[fg=green,dim]|#[default] load:#(cut -d' ' -f1 /proc/loadavg)#[default]"
# Datetime.
# `%a` depends on locale.
#set -qga status-right " <%Y-%m-%d(%a) %H:%M"
set -qga status-right " <%Y-%m-%d %H:%M"
# Battery capacity (percentage) if available.
if "test -f /sys/class/power_supply/BAT0/capacity" \
    'set -qga status-right " / #(cat /sys/class/power_supply/BAT0/capacity)%"'

# Trim the right-side component when too long.
set -qg status-right-length 256

# Key bindings. {{{1

# Basic bindings. {{{2

# Use vi-style key binding in copy and choice modes.
set -wqg mode-keys vi
# Use vi-style key binding in status line.
set -wqg status-keys vi

# Prefix key. {{{2

# Unbind the default prefix key `C-b`.
unbind C-b
# Set the prefix to `C-t`.
set -qg prefix C-t
# Type `^T` 2 times to send `^T` itself.
bind C-t send-prefix

# Windows and panes. {{{2

# Pane selection with vi-like keys.
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Move to adjacent windows with `Ctrl-arrow`.
unbind C-Left
bind -n C-Left prev
unbind C-Right
bind -n C-Right next

# Resize pane with vi-like keys.
# Note that `+`/`-` works diferrently to vim.
# They moves pane border up and down.
bind -r > resize-pane -R
bind -r < resize-pane -L
bind -r + resize-pane -U
bind -r - resize-pane -D

# Following pane creation settings are useful for >=tmux-1.9.
# On tmux-1.9 or newer, current directory of a new window or a new pane is `$HOME` by default.

# Open new window with current directory of current pane.
bind c neww -c "#{pane_current_path}"
# Create new pane with current directory of current pane.
bind % splitw -h -c "#{pane_current_path}"
# Create new pane with current directory of current pane.
bind '"' splitw -v -c "#{pane_current_path}"

# Other. {{{2

# Force reflesh
bind C-l refresh-client

# Reload config
bind C-r source ~/.tmux.conf

# Common settings. {{{1

# Number of buffers (which is used to copy and paste).
set -qg buffer-limit 32
# Number of window history (scrollback buffer).
set -qg history-limit 24000

# Set delay after `Esc` keypress to 0 .
# See <http://superuser.com/questions/252214/slight-delay-when-switching-modes-in-vim-using-tmux-or-screen>.
set -qsg escape-time 0

# Enable aggressive resize
setw -g aggressive-resize on

# Word separator in copy-mode.
set -qg word-separators '-_@/ :=,.'

# Terminal capabilities. {{{2

# Disable auto renaming of windows.
set -wqg automatic-rename off
# Enable window title change.
set -qg set-titles on

## Set default terimnal (the default value of TERM environment variable).
set -g default-terminal "tmux-256color"

# Support 24-bit true colors.
set-option -ga terminal-overrides ",*:RGB"
# Draw ruled lines using ACS instead of UTF-8 line characters.
#set-option -as terminal-overrides ",*:U8=0"

# Set NCURSES_NO_UTF8_ACS to 0 if borders are shown as 'qqqqq'...
# You can print borders with `dialog`, e.g. `dialog --msgbox hello 10 40`.
#setenv -g NCURSES_NO_UTF8_ACS 0

# Enable altername screen feature.
set -qg alternate-screen on

# Bell settings. {{{2

# Let bell pass through only for current window.
set -qg bell-action current

# Turn off visual bell.
set -qg visual-bell off

# vim: set foldmethod=marker expandtab tabstop=4 :
